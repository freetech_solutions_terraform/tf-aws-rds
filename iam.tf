data "template_file" "enhanced_monitoring_role_tpl" {
  template = file("${path.module}/templates/enhanced_monitoring_role.tpl")
}

data "template_file" "enhanced_monitoring_policy_tpl" {
  template = file("${path.module}/templates/enhanced_monitoring_policy.tpl")
}


resource "aws_iam_role" "enhanced_monitoring" {
  name_prefix        = "${var.name}-rds-em-"
  assume_role_policy = data.template_file.enhanced_monitoring_role_tpl.rendered

}

resource "aws_iam_role_policy" "enhanced_monitoring" {
  name   = "${var.name}-rds-em"
  role   = aws_iam_role.enhanced_monitoring.id
  policy = data.template_file.enhanced_monitoring_policy_tpl.rendered

}

