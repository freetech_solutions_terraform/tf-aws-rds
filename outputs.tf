// ID of the master instance
output "id" {
  value       = aws_db_instance.master.id
  description = "Id of the master instance"
}

// ARN of the master instance
output "arn" {
  value       = aws_db_instance.master.arn
  description = "ARN of the master instance"
}

// Port of the master instance
output "port" {
  value       = aws_db_instance.master.port
  description = "Port of the master instance"
}

// FQDN of the master instance
output "address" {
  value       = aws_db_instance.master.address
  description = "FQDN of the master instance"
}

// The connection endpoint of the master instance
output "endpoint" {
  value       = aws_db_instance.master.endpoint
  description = "The connection endpoint of the master instance"
}

output "replica_address" {
  value       = join(",", aws_db_instance.replica.*.address)
  description = "FQDN of the replica instance(s)"
}

// The connection(s) endpoint(s) of the slave(s) instance(s)
output "replica_endpoints" {
  value       = join(",", aws_db_instance.replica.*.endpoint)
  description = "The connection(s) endpoint(s) of the slave(s) instance(s)"
}

// Output for resource dependency, will output as soon as the RDS Master and Replica(s) Instances are ready
output "rds_db_ready" {
  value       = null_resource.rds_db_ready.id
  description = "Output for resource dependency, will output as soon as the Instances are ready"
}

