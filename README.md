# tf-aws-rds

## Overview

Creates an RDS master instance with optional replicas.

## Usage:
```
module "rds" {
  source                     = "../modules/tf-aws-rds"

  subnet_ids                 = ["subnet-69bddd0c", "subnet-f759faae", "subnet-61b47b4a"]
  vpc_id                     = "vpc-0cef8b69"
  name                       = "my-rds-instance"
  username                   = "mariadb"
  password                   = "mariadbs"
  engine                     = "mariadb"
  backup_retention_period    = 5
  security_group_ids         = ["sg-06406e22352d30735"]

  replica_count              = 1
  replica_security_group_ids = ["ssg-06406e22352d30735"]
  tags    = module.tags.tags

}
```

## Inputs

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| name | Name of the DB | - | yes |
| username | Root username. | - | yes |
| password | Initial password for the root user. | - | yes |
| vpc_id | The ID of the VPC where the instance will live | - | yes |
| subnet_ids | A list of subnet IDs | - | yes |
| security_group_ids | A list of Security Groups that will be assigned to the mastar instance | - | yes |
| parameter_group_name | Id of the parameter group for the engine of RDS | `""` | no |
| db_name | Standard Name of the Database | `""` | no |
| storage | How many GBs will be provisioned | `100` | no |
| storage_type | Storage type | `"gp2"` | no |
| provisioned_iops | Provisioned IOPS for storage type io1 | `0` | no |
| engine | Engine type | `"mysql"` | no |
| mysql_engine_version | Default MySQL Engine version | `"5.7.16"` | no |
| mariadb_engine_version | Default MariaDB Engine version | `"10.1.19"` | no |
| postgres_engine_version | Default PostgreSQL Engine version | `"9.6.1"` | no |
| master_instance_class | Master Instance class | `"db.m4.xlarge"` | no |
| replica_instance_class | Replica Instance class | `"db.m4.xlarge"` | no |
| multi_az | If you want the instance to be Multi-AZ or not | `true` | no |
| master_publicly_accessible | If the master instance will be publicly accessible | `false` | no |
| snapshot_identifier | Create and RDS instance from RDS snapshot | `""` | no |
| skip_final_snapshot | Determines whether a final DB snapshot is created before the DB instance is deleted. | `true` | no |
| final_snapshot_identifier | The name of your final DB snapshot when this DB instance is deleted. | `"rds-snapshot"` | no |
| copy_tags_to_snapshot | On delete, copy all Instance tags to the final snapshot | `true` | no |
| apply_immediately | If the changes should be inmmedately applied | `true` | no |
| mysql_port | MySQL Default Listening Port | `3306` | no |
| postgres_port | PostgreSQL Default Listening Port | `5432` | no |
| backup_window | Backup window | `"01:00-01:30"` | no |
| maintenance_window | Maintenance window date | `"tue:01:30-tue:02:00"` | no |
| monitoring_interval | The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance | `1` | no |
| backup_retention_period | Retention period | `7` | no |
| replica_count | How many replicas should be initialized | `0` | no |
| replica_publicly_accessible | Should the replica instance will be publicly accessible | `false` | no |
| replica_security_group_ids | A list of Security Groups that will be assigned to the slave instance | - | yes |
| auto_minor_version_upgrade | Minor version upgrade for Master | false | no |
| allow_major_version_upgrade | Major version upgrade for Master | false | no |
| replica_auto_minor_version_upgrade | Minor version upgrade for Replica | false | no |
| replica_allow_major_version_upgrade | Major version upgrade for Replica | false | no |
| tags | A mapping of tags to assign to the resource | - | yes |
| rds_timeouts | A mapping of required timeouts for create/delete/update operations | 40m/80m/40m for each | no |

## Outputs

| Name | Description |
|------|-------------|
| id | Id of the master instance |
| arn | ARN of the master instance |
| port | Port of the master instance |
| address | FQDN of the master instance |
| endpoint | The connection endpoint of the master instance |
| replica_address | FQDN of the replica instance(s) |
| replica_endpoints | The connection(s) endpoint(s) of the slave(s) instance(s) |
| rds_db_ready | Output for resource dependency, will output as soon as the RDS Master and Replica(s) Instances are ready |
